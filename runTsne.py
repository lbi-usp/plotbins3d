import os, sys, csv, numpy as np

def runTsne(bhtsneDir, outDir, dataFile, initDimension, dimension, perplexity, theta, overwrite = 0, verbose = 0):
   if not os.path.isdir(outDir):
      os.mkdir(outDir)
   outFile = os.path.join(outDir, "allContigs_3d.tsv")
   if not os.path.isfile(outFile) or overwrite:
      command = "python %s/bhtsne.py -i %s -o %s -n %i -d %i -p %f -t %f -v"%(bhtsneDir, dataFile, outFile, initDimension, dimension, perplexity, theta)
      if verbose: print "Rodando: ", command
      os.system(command)
   else:
      if verbose: print "Output file %s already exists and overwrite = 0"%outFile
   return outFile
