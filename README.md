# Plotando bins em 3D #

Script para plotar um conjunto de contigs em 3D. Cada contig é desenhado como uma bolinha no espaço 3D. Contigs do mesmo arquivo são plotados com a mesma cor, enquanto que diferentes arquivos de contigs possuem cores diferentes.

### Como instalar ###

Faça o download (*git clone https://lbi-usp@bitbucket.org/lbi-usp/plotbins3d.git*) do repositório. O arquivo principal é "plotBins.py", para obter ajuda sobre os parâmetros que ele recebe execute:

```
#!python

python plotBins.py -h 
```

### Dependências ###

O script usa uma implementação do algoritmo t-SNE de Barnes-Hut (https://github.com/lvdmaaten/bhtsne). Esse software está no diretório **bhtsne**. Para compilar o bhtsne siga as instruções em https://github.com/lvdmaaten/bhtsne. Basicamente entre na pasta *bhtsne* e rodar 

```
#!bash
g++ sptree.cpp tsne.cpp -o bh_tsne -O2

```

Outras dependências são listadas a seguir:

* [numpy](http://www.numpy.org/)
* [plotly](https://plot.ly/) para criar gráficos 3D mais interativos.
* [matplotlib](http://matplotlib.org/) para criar gráficos 3D estáticos.
* [ckeckm](https://github.com/Ecogenomics/CheckM/wiki) para calcular o barcodes dos contigs.

### Como usar ###

Você tem um conjunto de bins, onde cada bin é um arquivo **.fasta**, **.fa** ou **.faa** com os contigs que o formam. O script recebe como entrada o diretório onde estão os bins e o diretório de saída onde plotará os resultados. O script encontra todos os arquivos no diretório de origem e cria o(s) gráfico(s) 3D no diretório de saída. 

Existem duas opções de plotagem:

* Criar um gráfico interativo (usando **plotly**), essa opção gera um arquivo .html no diretório de saída que pode ser aberto em um navegador para explorar os bins. Todos os contigs do mesmo bin são desenhados com a mesma cor e quando passa o mouse por cima de um ponto, é mostrado o nome do contig, o nome do bin ao qual ele pertence, assim como as coordenadas (X, Y, Z). O nome de cada bin mostrado na legenda do gráfico é o mesmo que o nome do arquivo que contem os contigs desse bin. 
* Criar vários gráficos 3D estáticos, mudando o ângulo de visualização. Os gráficos são criados usando **matplotlib**, todos os contigs do mesmo bin possuem a mesma cor. Nessa primeira versão do script, o ângulo de visualização muda de 0 a 360 graus com step de 5 graus.

O arquivo *config.py* possue alguns parâmetros configuráveis como o caminho onde se encontra o executável do **bhtsne** (por padrão é *./bhtsne*) e os parâmetros para chamar esse algoritmo.

 Exemplo de uso:


```
#!python
python plotBins.py -i myBinsDir -o myBins_plotDir 
```

No diretório de saída o script deixa (de propósito) arquivos com informações intermediarias, como o barcodes de cada bin como fornecido pelo **ckeckM**, os mesmos arquivos de barcodes mas sem cabeçalhos, assim como um arquivo com todos os barcodes de todos os contigs existentes em todos os bins. Também há um arquivo onde cada contig é representado por 3 valores, depois de rodar o algoritmo t-SNE. 

Se o script é rodado repetidas vezes com a mesma entrada e o mesmo diretório de saída, ele "evitará" sobrescrever os arquivos existentes. O motivo é para evitar "refazer" o trabalho já feito e que pode demorar dependendo do tamanho da entrada. Isso é útil por exemplo, quando mudamos apenas o tipo de gráfico que queremos gerar, pois a informação sobre cada contig é a mesma e não tem de ser recalculada. Mas é possível forçar o script a refazer o trabalho todo (sobrescrevendo os arquivos de saída que porventura existirem) com a opção *-ow=1* (*overwrite*). Se acontecer algum erro durante a execução do script, ative a função overwrite para calcular os dados desde zero.

### Como é calculado o ponto 3D de cada contig ###

Seguimos a ideia do MyCC (http://www.nature.com/articles/srep24175), que usa o algoritmo t-SNE para reduzir a dimensão do barcodes dos contigs de 136 para 2 e plota os contigs em 2D, mas a diferença nosso script plota os contigs em três dimensões ao invés de duas.

A grosso modo, o script faz o seguinte:

* Calcula o barcodes para cada contig de cada bin usando o *checkm tretra*. O resultado é um arquivo separado por tab onde cada linha representa um contig, e cada coluna a frequência relativa de cada tetra nucleotídeo nesse contig. Obtemos 1 arquivo por cada bin.
* Junta o barcodes de todos os bins em um arquivo (removendo a primeira linha e primeira coluna, que são cabeçalhos) e chama o **bhtsne** para reduzir a dimensão de 136 para 3. Os parâmetros usados na chamada do **bhtsne** são os mesmos usados no artigo do MyCC (veja ref. acima). Os parâmetros são: *perpexlity = 20.0* e *theta = 0.5*.
* O arquivo resultante possui 3 colunas (os componentes "principais") para cada contig. Cada contig é identificado pela ordem em que foram empilhados no arquivo grande que foi passado ao **bhtsne**. Depois os gráficos são criados.

### TODO list ###

Algumas características que poderiam ser adicionadas:

* Adicionar a opção de aplicar uma transformação *centered log-ratio* aos dados antes de plotar.
* Adicionar a opção de usar PCA ao invés de t-SNE para reduzir a dimensão dos contigs (qual a diferença entre t-SNE e PCA?).
* No gráfico interativo (criado com **plotly**), quando clica em um contig, adicionar ele a uma lista, o que seria útil para filtragem manual de "outliers".
* Adicionar a opção de detectar outliers automaticamente usando métodos estatísticos.

Problemas ou dúvidas: [diaztula@iq.usp.br](mailto:diaztula@iq.usp.br)