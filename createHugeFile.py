import csv, os, sys, numpy as Math

def createHugeFile(srcDir, overwrite = 0, verbose = 0):
   if not os.path.isdir(srcDir):
      print "Diretorio fonte %s nao existe"%srcDir
      sys.exit(1)
   #outDir = os.path.join( srcDir[0:srcDir.rfind("/")], "allContigs_no_header.tsv" )
   outDir = os.path.join(srcDir, "allContigs_no_header.tsv")
   if not os.path.isfile(outDir) or overwrite:
      if verbose and overwrite: print "Creating (or overwriting) output file %s"%outDir
      # Finding all .tsv files in source
      files = [f for f in os.listdir(srcDir) if os.path.isfile(os.path.join(srcDir, f)) and f.endswith("_no_header.tsv") and not "allContigs" in f]
      files.sort()
      if verbose: print "Criando arquivo para rodar o t-SNE binario"
      firstF = files[0]
      X = Math.genfromtxt(os.path.join(srcDir, firstF), delimiter="\t", skip_header=0)
      if verbose: print "Carregei dados do arquivo %s com %i contigs"%(firstF, X.shape[0])
      indexes = [0, X.shape[0]]
      for i, f in enumerate(files[1::]):
         Xf = Math.genfromtxt(os.path.join(srcDir, f), delimiter="\t", skip_header=0)
         # Path needed when we have only 1 contigs (concoct clusters can have only 1 read)
         if len(Xf.shape) == 1:
            print X.shape[1]
            Xf = Math.array(Xf).reshape(1, X.shape[1])
         print Xf.shape, Xf.shape[0], Xf.shape[1]
         if verbose: print "Carregei dados do arquivo %s com %i contigs"%(f, Xf.shape[0])
         indexes.append(Xf.shape[0] + indexes[-1])
         X = Math.concatenate((X, Xf), axis=0)
         if verbose: print "Total de contigs carregados: %i com dimensao %i"%(X.shape[0], X.shape[1])
      outFile = open(outDir, "wb")
      outCsv = csv.writer(outFile, delimiter = "\t")
      outCsv.writerows(X)
   else:
      if verbose: print "Output file: %s already exists and overwrite is false"%outDir
   return outDir

if __name__ == "__name__":
   if len(sys.argv) != 2:
      print "Uso: python %s source_dir"%sys.argv[0]
      sys.exit(1)
   srcDir = sys.argv[1]
   createHugeFile(srcDir)
   
