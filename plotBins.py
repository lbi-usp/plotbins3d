import sys, os, argparse
sys.path.append(".")
from config import *
sys.path.append(tsnePath)

import createSignatures, createHugeFile, runTsne#, plotResults3D

def parseArgs(args):
    argp = argparse.ArgumentParser()
    argp.add_argument('-i', help = 'Dir with the contigs files', required = True)
    argp.add_argument('-o', help = 'Dir to write the results'  , required = True)
    argp.add_argument('-ow', help = 'Overwrite existing files in the process (default = 0)', type = int, default = 0, required = False)
    argp.add_argument('-vb', help = 'Verbose mode (default = 1)', type = int, default = 0, required = False)
    argp.add_argument('-interactive', help = 'Create interactive plot with plotly (0 means use matplotlib to create the plots, default=1)', type = int, default = 1, required = False)
    return argp.parse_args(args)

if __name__ == "__main__":
    print "<=====================================================>"
    print "This script creates a 2D plot of bins of contigs"
    args = parseArgs(sys.argv[1:])
    print "Dir with the contigs files: %s"%args.i
    print "Dir to write the results  : %s"%args.o
    print "<---- Step 1: compute genomic signatures for each contig ---->"
    initDimension = createSignatures.createSignatures(args.i, args.o, args.ow, args.vb)
    print "Step 1 done."
    print "<---- Step 2: create huge file with all contigs and call bh_tsne ---->"
    hugeFile = createHugeFile.createHugeFile(args.o, args.ow, args.vb)
    file3d   = runTsne.runTsne(tsnePath, args.o, hugeFile, initDimension, dimension, perplexity, theta, args.ow, args.vb)
    print "Step 2 done."
    print "<---- Step 3: plotting the results ---->"
    if args.interactive == 1:
        import plot3D_plotly
        plot3D_plotly.plotResults(args.o, args.o, file3d, args.ow, args.vb)
    else:
        import plot3D_mpl
        plot3D_mpl.plotResults(args.o, args.o, file3d, args.ow, args.vb)
    print "Step 3 done."
    print "<=====================================================>"
