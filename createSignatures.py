import os, sys, csv, numpy as np

def createSignatures(srcDir, outDir, overwrite = 0, verbose = 0):
   if not os.path.isdir(srcDir):
      print "Source dir %s does not exists"%srcDir
      sys.exit(1)
   if not os.path.isdir(outDir):
      os.mkdir(outDir)
   dimension = 0
   # Finding all .fasta files in source
   files = [f for f in os.listdir(srcDir) if os.path.isfile(os.path.join(srcDir, f)) and (f.endswith(".fasta") or f.endswith(".fa") or f.endswith(".fna"))]
   files.sort()
   # Calling checkm for all files to detect genomic signatures
   for f in files:
      if verbose:
         print "========================================================================"
      fileExt = f[f.rfind("."):]
      outFile = os.path.join(outDir, f.replace(fileExt, ".tsv"))
      if not os.path.isfile(outFile) or overwrite:
         print "Calculando assinatura genomica de %s"%os.path.join(srcDir, f)
         if verbose: print "Gravando em %s"%outFile
         command = "checkm tetra %s %s"%(os.path.join(srcDir, f), outFile)
         if verbose: print "Rodando: ", command
         os.system(command)
         # Arrumando o arquivo para remover os cabecalos
         X = np.genfromtxt(outFile, delimiter="\t", skip_header=0)
         print X.shape
         X = np.delete(X, 0, 1)
         X = np.delete(X, 0, 0)
         dimension = X.shape[1]
         fixedOutFile = outFile.replace(".tsv", "_no_header.tsv")
         fixedOut = open(fixedOutFile, "wb")
         writer = csv.writer(fixedOut, delimiter = "\t")
         writer.writerows(X)
      else:
         if verbose: print "Assinatura genomica ja existe para %s e overwrite eh 0"%outFile
         # Compute dimension
         if dimension == 0:
            X = np.genfromtxt(outFile, delimiter="\t", skip_header=1)
            dimension = len(X[0]) - 1
      if verbose: print "========================================================================"
   return dimension

if __name__ == "__main__":
   if len(sys.argv) != 3:
      print "Uso: python %s source_dir out_dir"%sys.argv[0]
      sys.exit(1)
   srcDir = sys.argv[1]
   outDir = sys.argv[2]
   createSignatures(srcDir, outDir)
