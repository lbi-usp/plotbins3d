'''
Plot3D: creates a 3D plot of contigs by first running t-sne to reduce dimensionality
and then create an interactive plot with Plotly.

Copyright (C) 2016, Antonio Diaz Tula

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import math, csv, sys, os, numpy as np

def plotResults(srcDir, outDir, contigs2d, overwrite = 0, verbose = 0):
    if not os.path.isdir(srcDir):
        print "Diretorio fonte %s nao existe"%srcDir
        sys.exit(1)
    if not os.path.isfile(contigs2d):
        print "Arquivo con dados de todos os contigs em 2d nao existe"
        sys.exit(1)
    if not os.path.isdir(outDir):
        os.mkdir(outDir)
    # Finding all .fasta files in source
    ##### files = [f for f in os.listdir(srcDir) if os.path.isfile(os.path.join(srcDir, f)) and f.endswith("_no_header.tsv") and not "allContigs" in f]
    files = [f for f in os.listdir(srcDir) if os.path.isfile(os.path.join(srcDir, f)) and f.endswith(".tsv") and not "_no_header" in f and not "allContigs" in f]
    files.sort()
    # Counting the number of contigs for each bin
    indexes = [0]
    labels  = []
    text    = []
    for f in files:
        labels.append(f.replace("_no_header.tsv", ""))
        reader = csv.reader(open(os.path.join(srcDir, f), "rt"), delimiter = "\t")
        sumsum = 0
        reader.next()
        for line in reader:
            text.append(line[0])
            sumsum += 1
        contigsCount = sumsum#len(list(reader))-1
        indexes.append(contigsCount + indexes[-1])
    # Loading and plotting 
    allPoints = np.array(list(csv.reader(open(contigs2d, "rt"), delimiter = "\t")), dtype = np.float64)
    # Loading and customizing matplotlib
    import matplotlib as mpl
    mpl.use('Agg')

    # Plotando em 3D com plotly
    import plotly
    import plotly.graph_objs as go
    from plotly.widgets import GraphWidget

    #import cufflinks as cf
    #cf.set_config_file(offline=True, world_readable=True, theme='ggplot')
    
    data = []
    # Computing plot limits
    ## minx = np.floor(np.amin(np.array(allPoints[:,0], dtype = np.float64))) - 1
    ## maxx = np.ceil (np.amax(np.array(allPoints[:,0], dtype = np.float64))) + 1
    ## miny = np.floor(np.amin(np.array(allPoints[:,1], dtype = np.float64))) - 1
    ## maxy = np.ceil (np.amax(np.array(allPoints[:,1], dtype = np.float64))) + 1
    ## minz = np.floor(np.amin(np.array(allPoints[:,2], dtype = np.float64))) - 1
    ## maxz = np.ceil (np.amax(np.array(allPoints[:,2], dtype = np.float64))) + 1
    ## plot.set_xlim(minx, maxx)
    ## plot.set_ylim(miny, maxy)
    # Creating color maps
    import matplotlib.pyplot as plt
    cmap = plt.get_cmap('Paired')
    colors = [cmap(i) for i in np.linspace(0, 1, len(files))]
    
    # Plotting each contig with its correponding color and label
    totalCount = 0
    for i, f in enumerate(files):
        """
        figBin = plt.figure(figsize=(15,15))
        plotBin = figBin.add_subplot(111, projection='3d')
        plotBin.set_xlim(minx, maxx)
        plotBin.set_ylim(miny, maxy)
        """
        c = colors[i] # next(colors)
        i0 = indexes[i]; i1 = indexes[i+1]
        if verbose: print "Para o bin %s pegamos do indice %i ate o %i com cor [%s]"%(f, i0, i1, ",\t".join( [str(cc) for cc in c] ))
        points = allPoints[i0:i1][:]
        totalCount += points.shape[0]
        rgbColor = 'rgb(%s)'%(",".join([str(int(j*255)) for j in c[0:3]]))
        print "Plotting in 3d: %s with color %s"%(labels[i], rgbColor)
        marker=dict(color=rgbColor, size=6, symbol='circle', line=dict(color='rgb(0, 0, 0)', width=0.5))
        trace = go.Scatter3d(x=points[:,0], y=points[:,1], z=points[:,2], name=labels[i], mode='markers', marker = marker, opacity=1.0, text=text[i0:i1])
        data.append(trace)
    layout = go.Layout(margin=dict(l=0, r=0, b=0, t=0))
    url = plotly.offline.plot(data, layout, filename = "%s/%s.html"%(srcDir, srcDir.replace("/", "_").replace(".", "")))
    print url
    
    
if __name__ == "__main__":
    # Validating parameters
    if len(sys.argv) != 3:
        print "Uso: python %s signatures_dir all_contigs_2d_file"%sys.argv[0]
        sys.exit(1)
    srcDir     = sys.argv[1]
    contigs2d  = sys.argv[2]
    outDir     = srcDir
    plotResults(srcDir, outDir, contigs2d, overwrite = 0, verbose = 0)
    
    
